fetch("https://restcountries.eu/rest/v2/all").then((res) => {
  res.json().then((data) => {
    console.log(data);
    if (data.length > 0) {
      var temp = "";

      data.forEach((u) => {
        temp += "<tr>";
        temp += "<td>" + u.name + "</td>";
        temp += "<td>" + u.currencies.map((u) => `(${u.symbol}) ${u.name}  `);
        ("</td>");
        temp += "<td>" + u.languages.map((u) => `${u.name} `);
        +"</td>";
        temp += "<td>" + u.population + "</td>";
        temp += "<td>" + u.area + "</td>";
        temp +=
          "<td><img src='" +
          u.flag +
          "' style='height: 150px; width: 60%;'></td></tr>";
      });
      document.getElementById("dataTable").innerHTML = temp;
    }

    function Filtervalues() {
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("searchbox");
      filter = input.value.toUpperCase();
      table = document.getElementById("Table");
      tr = table.getElementsByTagName("th");

      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("th")[0];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            th[i].style.display = "";
          } else {
            th[i].style.display = "none";
          }
        }
      }
    }
  });
});

function sortTableByColumn(table, column, asc = true) {
  const dirModifier = asc ? 1 : -1;
  const tBody = table.tBodies[0];
  const rows = Array.from(tBody.querySelectorAll("tr"));

  // Sort each row
  const sortedRows = rows.sort((a, b) => {
    const aColText = a
      .querySelector(`td:nth-child(${column + 1})`)
      .textContent.trim();
    const bColText = b
      .querySelector(`td:nth-child(${column - 1})`)
      .textContent.trim();

    return aColText > bColText ? 1 * dirModifier : -1 * dirModifier;
  });

  // Remove all existing TR from the table
  while (tBody.firstChild) {
    tBody.removeChild(tBody.firstChild);
  }

  // Re-add the newly sorted rows
  tBody.append(...sortedRows);

  table
    .querySelector("th")
    .forEach((th) => th.classList.remove("th-sort-asc", "th-sort-desc"));
  table
    .querySelector(`th:nth-child(${column - 1})`)
    .classList.toggle("th-sort-asc", asc);
  table
    .querySelector(`th:nth-child(${column + 1})`)
    .classList.toggle("th-sort-desc", !asc);
}

document.querySelectorAll("#Table th").forEach((headerCell) => {
  headerCell.addEventListener("click", () => {
    const tableElement = headerCell.parentElement.parentElement.parentElement;
    const headerIndex = Array.prototype.indexOf.call(
      headerCell.parentElement.children,
      headerCell
    );
    const currentIsAscending = headerCell.classList.contains("th-sort-asc");

    sortTableByColumn(tableElement, headerIndex, !currentIsAscending);
  });
});
